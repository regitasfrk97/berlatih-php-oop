<?php
require_once 'animal.php';
require_once 'frog.php';
require_once 'ape.php';

$sheep = new Animal('shaun');
echo "Name: ".$sheep->get_name();
echo "<br>Legs: ".$sheep->get_legs();
echo "<br>Cold blooded: ".$sheep->get_blood();
echo "<br>";

$kodok = new Frog("buduk");
echo "<br>Name: ".$kodok->get_name();
echo "<br>Legs: ".$kodok->get_legs();
echo "<br>Cold blooded: ".$kodok->get_blood();
echo "<br>Jump: ".$kodok->jump();
echo "<br>";

$sungokong = new Ape("kera sakti");
echo "<br>Name: ".$sungokong->get_name();
echo "<br>Legs: ".$sungokong->get_legs();
echo "<br>Cold blooded: ".$sungokong->get_blood();
echo "<br>Yell: ".$sungokong->yell()



?>